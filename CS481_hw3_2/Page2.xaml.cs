﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_hw3_2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]




    public partial class Page2 : ContentPage
    {




        // convert to class with get,set for list population

        public struct Contact
        {
            public String Name;
            public String Email;
            public String Phone;
        };


        //states
        bool hasName = false;
        bool hasEmail = false;
        bool hasPhone = false;

        int index = 0;

        //page global 
        List<Contact> contacts;
        Contact currentContact;


        //prepopulate db with data for sample
        private void PreloadDB()
        {
            Contact johnd = new Contact();
            johnd.Email = "john@doe.com";
            johnd.Phone = "123456789";
            johnd.Name = "John Doe";

            contacts.Add(johnd);


            Contact janed = new Contact();
            janed.Email = "jane@doe.com";
            janed.Phone = "123456789";
            janed.Name = "Jane Doe";
            contacts.Add(janed);


        }

        public Page2()
        {

            contacts = new List<Contact>();
            currentContact = new Contact();

            InitializeComponent();
            //ContactList.ItemsSource=contacts;


        }




        //add complete entry to list
        private void AddEntry(object sender, EventArgs e)
        {

            String message = "Please Complete: ";
            if (NameEntry.Text == "")
            {
                message += "Name ";

            }
            else
            {
                currentContact.Name = NameEntry.Text;

                hasName = true;
            }
            if (EmailEntry.Text == "")
            {
                message += "Email ";
            }
            else
            {
                currentContact.Email = EmailEntry.Text;
                hasEmail = true;
            }

            if (PhoneEntry.Text == "")
            {
                message += "Phone ";
            }
            else
            {
                currentContact.Phone = PhoneEntry.Text;

                hasPhone = true;
            }


            if (hasName == true && hasEmail == true && hasPhone == true)
            {
                saveEntry();
            }
            else
            {
                DisplayAlert("Incomplete Contact", message, "OK");
            }
        }

        private void saveEntry()
        {


            if (hasName == true && hasEmail == true && hasPhone == true)
            {
                hasName = hasEmail = hasPhone = false;
                //copy or reference?
                contacts.Add(currentContact);


                currentContact = new Contact();
                NameEntry.Text = "";
                EmailEntry.Text = "";
                PhoneEntry.Text = "";



                DisplayContact(contacts.Count - 1);

            }
        }
        private void DisplayContact(int num)
        {
            if ((contacts.Count >= 1) && (contacts.Count > num))
            {
                Contact cur = contacts.ElementAt(num);
                PhoneView.Text = cur.Phone;
                EmailView.Text = cur.Email;
                NameView.Text = cur.Name;
            }
        }

        public void NextContact(object sender, EventArgs e)
        {

            index++;
            if (index >= contacts.Count)
            {
                index = 0;
            }

            DisplayContact(index);
        }

        public void PrevContact(object sender, EventArgs e)
        {
            index--;
            if (index < 0)
            {
                index = contacts.Count - 1;
            }

            DisplayContact(index);
        }
        //check data entries

        private void NameEntered(object sender, EventArgs e)
        {
            if (NameEntry.Text != "")
            {
                currentContact.Name = NameEntry.Text;
                hasName = true;
            }

            saveEntry();

        }
        private void EmailEntered(object sender, EventArgs e)
        {
            if (EmailEntry.Text != "")
            {
                currentContact.Email = EmailEntry.Text;
                hasEmail = true;
            }
            saveEntry();
        }
        private void PhoneEntered(object sender, EventArgs e)
        {
            if (PhoneEntry.Text != "")
            {
                currentContact.Phone = PhoneEntry.Text;
                hasPhone = true;
            }
            saveEntry();

        }


        //handle appearing
        void OnAppearing(object sender, EventArgs e)
        {

            //preload data
            if (contacts.Count == 0)
            {
                PreloadDB();
                index = 0;
            }

            //fetch last index
            if (Application.Current.Properties.ContainsKey("contactIndex") == true)
            {
                index = (int)Application.Current.Properties["contactIndex"];
            }


            if (contacts.Count != 0)
            {
                if (index >= contacts.Count || index < 0)
                    index = 0;
                DisplayContact(index);
            }



        }

        //handle appearing
        void OnDisappearing(object sender, EventArgs e)
        {

            Application.Current.Properties["contactIndex"] = index;


        }

    

    }

}