﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_hw3_2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {

        //media index
        private int index = 0;
        private struct Media
        {
            public String title;
            public String ImageDescription;
            public String Image;
            public string youtubelink;
            public string SliderDescription;
            public string next;
            //public EventHandler action;  //compounds commands
            public bool action; //use 2 buttons instead with enable

        }

        private List<Media> media = new List<Media>();



        public Page3()
        {
            mediaData();
            InitializeComponent();

        }


        public void mediaData()
        {

            //Wintergatan Marble Machine
            Media wg = new Media();
            wg.title = "Marble Machine";
            wg.ImageDescription = "Click Image if Bored";
            wg.Image = "WGmaxresdefault.jpg";
            wg.youtubelink = "https://www.youtube.com/watch?v=wlDEC0Ln4vI";
            wg.SliderDescription = "Slide to Roll";
            wg.next = "Tired of Marbles?";
            wg.action = true;
            //compunds actions 
            //wg.action = new EventHandler(NextMedia);

            media.Add(wg);

            //rick astley
            Media rick = new Media();
            rick.title = "Rick Astley";
            rick.ImageDescription = "Click Image for Bonus Feature";
            rick.Image = "maxresdefault.jpg";
            rick.youtubelink = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
            rick.SliderDescription = "Move Slider to make Rick Roll";
            rick.next = "Tired of Rick?";
            rick.action = true;
            //rick.action = new EventHandler(NextMedia);
            media.Add(rick);

            Media rick2 = new Media();
            rick2.title = "Never Gonna Give You Up";
            rick2.ImageDescription = "Click Image for Bonus Feature";
            rick2.Image = "maxresdefault.jpg";
            rick2.youtubelink = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
            rick2.SliderDescription = "Move Slider to make Rick Roll";
            rick2.next = "Tired of Media?";
            rick2.action = true;
            //rick2.action = new EventHandler( ReturnRoot) ;


            media.Add(rick2);

        }
        private void updatePage(int num)
        {
            if (num < media.Count)
            {
                Media current = media.ElementAt(num);
                Title = current.title;
                ImageDisc.Text = current.ImageDescription;
                Image.Source = current.Image;
                SliderDisc.Text = current.SliderDescription;
                PageButton.Text = current.next;
                PageButton.IsEnabled = current.action;
                PageButton.IsVisible = current.action;

              
            }

        }

        private void Rotation_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            Image.Rotation = Rotation.Value * 1280;
            Image.RotationX = Rotation.Value * 360;
            Image.RotationY = Rotation.Value * 980;
        }

        //launch youtube video        
        private void PlayVideo(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri(media.ElementAt(index).youtubelink));
        }

        private void NextMedia(object sender, EventArgs e)
        {

            ++index;

            if (index >= media.Count)
            {
                index = 0;
            }
            updatePage(index);
        }
        /* not valid for tabbed pages
        async void ReturnRoot(object sender, EventArgs e)
        {
            index = 0;
            await Navigation.PopToRootAsync();
        }*/

        private void OnAppearing(object sender, EventArgs e)
        {
            index = 0;
            //fetch lastMedia property from Application properties dictionary
            if (Application.Current.Properties.ContainsKey("lastMedia"))
            {
                //cast property to int to be used
                index = (int)Application.Current.Properties["lastMedia"];
            }
            //load last/first media
            updatePage(index);
        }

        private void OnDisappearing(object sender, EventArgs e)
        {
            //save current index to lastMedia in application properties
            Application.Current.Properties["lastMedia"] = index;
        }
    }
}